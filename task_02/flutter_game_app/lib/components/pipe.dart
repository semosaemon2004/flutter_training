import 'package:flame/collisions.dart';
import 'package:flame/components.dart';
import 'package:flame/flame.dart';
import 'package:flutter_game_app/game/images.dart';
import 'package:flutter_game_app/game/my_game.dart';
import 'package:flutter_game_app/game/pipe_position.dart';
import 'package:flutter_game_app/game/settings.dart';

class Pipe extends SpriteComponent with HasGameRef<MyGame> {
  Pipe({
    required this.pipePosition,
    required this.height,
  });

  @override
  final double height;
  final PipePosition pipePosition;

  @override
  Future<void> onLoad() async {
    final pipe = await Flame.images.load(Issets.pipe);
    final pipeRotated = await Flame.images.load(Issets.pipeRotated);
    size = Vector2(50, height);

    switch (pipePosition) {
      case PipePosition.top:
        position.y = 0;
        sprite = Sprite(pipeRotated);
        break;
      case PipePosition.bottom:
        position.y = gameRef.size.y - size.y - Settings.groundHeight;
        sprite = Sprite(pipe);
        break;
    }

    add(RectangleHitbox());
  }
}
