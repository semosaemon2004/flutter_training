import 'package:flame/components.dart';
import 'package:flame/flame.dart';
import 'package:flutter_game_app/game/images.dart';
import 'package:flutter_game_app/game/my_game.dart';

class Background extends SpriteComponent with HasGameRef<MyGame> {
  Background();

  @override
  Future<void> onLoad() async {
    final background = await Flame.images.load(Issets.backgorund);
    size = gameRef.size;
    sprite = Sprite(background);
  }
}
