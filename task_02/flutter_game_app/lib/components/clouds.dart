import 'package:flame/components.dart';
import 'package:flame/flame.dart';
import 'package:flame/parallax.dart';
import 'package:flutter_game_app/game/images.dart';
import 'package:flutter_game_app/game/my_game.dart';
import 'package:flutter_game_app/game/settings.dart';

class Clouds extends ParallaxComponent<MyGame> with HasGameRef<MyGame> {
  Clouds();

  @override
  Future<void> onLoad() async {
    final image = await Flame.images.load(Issets.clouds);
    position = Vector2(x, -(gameRef.size.y - Settings.cloudsHeight));
    parallax = Parallax([
      ParallaxLayer(
        ParallaxImage(image, fill: LayerFill.none),
      ),
    ]);
  }

  @override
  void update(double dt) {
    super.update(dt);
    parallax?.baseVelocity.x = Settings.gameSpeed;
  }
}
