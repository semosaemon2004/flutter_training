import 'package:flame/collisions.dart';
import 'package:flame/components.dart';
import 'package:flame/flame.dart';
import 'package:flame/parallax.dart';
import 'package:flutter_game_app/game/images.dart';
import 'package:flutter_game_app/game/my_game.dart';
import 'package:flutter_game_app/game/settings.dart';

class Ground extends ParallaxComponent<MyGame> with HasGameRef<MyGame> {
  Ground();

  @override
  Future<void> onLoad() async {
    final ground = await Flame.images.load(Issets.ground);
    parallax = Parallax([
      ParallaxLayer(
        ParallaxImage(ground, fill: LayerFill.none),
      ),
    ]);
    add(
      RectangleHitbox(
        position: Vector2(0, gameRef.size.y - Settings.groundHeight),
        size: Vector2(gameRef.size.x, Settings.groundHeight),
      ),
    );
  }

  @override
  void update(double dt) {
    super.update(dt);
    parallax?.baseVelocity.x = Settings.gameSpeed;
  }
}
