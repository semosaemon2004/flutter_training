class Issets {
  static const backgorund = 'background_night.png';
  static const ground = 'ground_night.png';
  static const clouds = 'clouds.png';
  static const pipe = 'pipe.png';
  static const pipeRotated = 'pipe_rotated.png';

  static const birdMidFlap = 'bird_midflap.png';
  static const birdUpFlap = 'bird_upflap.png';
  static const birdDownFlap = 'bird_downflap.png';

  static const gameOver = 'assets/images/gameover.png';
  static const menu = 'assets/images/menu_night.png';
  static const message = 'assets/images/message.png';

  static const flying = 'fly.wav';
  static const collision = 'collision.wav';
  static const point = 'point.wav';
}