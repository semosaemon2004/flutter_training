import 'package:flutter/material.dart';
import 'package:flutter_calendar_app/views/screens/table_calendar.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Calendar App',
      home:  TableCalendarScreen(),
    );
  }
}
