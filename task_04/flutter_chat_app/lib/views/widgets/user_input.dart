import 'package:flutter/material.dart';
import 'package:emoji_picker_flutter/emoji_picker_flutter.dart';

class UserInputWidget extends StatefulWidget {
  final TextEditingController messageInputController;
  final VoidCallback onSend;

  UserInputWidget({
    Key? key,
    required this.messageInputController,
    required this.onSend,
  }) : super(key: key);

  @override
  _UserInputWidgetState createState() => _UserInputWidgetState();
}

class _UserInputWidgetState extends State<UserInputWidget> {
  bool isEmojiVisible = false;

  void _toggleEmojiKeyboard() {
    setState(() {
      isEmojiVisible = !isEmojiVisible;
    });
  }

  void _onEmojiSelected(Emoji emoji) {
    widget.messageInputController
      ..text += emoji.emoji
      ..selection = TextSelection.fromPosition(
        TextPosition(offset: widget.messageInputController.text.length),
      );
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          padding: const EdgeInsets.symmetric(horizontal: 8),
          color: Colors.white,
          child: SafeArea(
            child: Row(
              children: [
                IconButton(
                  icon: Icon(Icons.emoji_emotions_outlined),
                  onPressed: _toggleEmojiKeyboard,
                ),
                Expanded(
                  child: TextField(
                    controller: widget.messageInputController,
                    decoration: InputDecoration(
                      hintText: 'Type your message...',
                      border: InputBorder.none,
                    ),
                  ),
                ),
                IconButton(
                  icon: Icon(Icons.send, color: Colors.deepPurple),
                  onPressed: widget.onSend,
                ),
              ],
            ),
          ),
        ),
        Offstage(
          offstage: !isEmojiVisible,
          child: SizedBox(
            height: 250,
            child: EmojiPicker(
              onEmojiSelected: (category, emoji) => _onEmojiSelected(emoji),
              config: const Config(
                columns: 7,
                buttonMode: ButtonMode.MATERIAL,
              ),
            ),
          ),
        ),
      ],
    );
  }
}
