import 'package:flutter/material.dart';
import 'package:flutter_chat_app/providers/home.dart';
import 'package:flutter_chat_app/views/widgets/chat_message_tile.dart';
import 'package:provider/provider.dart';

class MessagesListWidget extends StatelessWidget {
  final String username;

  MessagesListWidget({Key? key, required this.username}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: Consumer<HomeProvider>(
        builder: (_, provider, __) => ListView.builder(
          padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 8),
          itemCount: provider.messages.length,
          itemBuilder: (context, index) {
            final message = provider.messages[index];
            bool isMe = message.senderUsername == username;
            return ChatMessageTileWidget(message: message, isMe: isMe);
          },
        ),
      ),
    );
  }
}
