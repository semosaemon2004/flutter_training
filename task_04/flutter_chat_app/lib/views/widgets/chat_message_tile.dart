import 'package:flutter/material.dart';
import 'package:flutter_chat_app/model/message.dart';
import 'package:intl/intl.dart';

class ChatMessageTileWidget extends StatelessWidget {
  final Message message;
  final bool isMe;

  ChatMessageTileWidget({required this.message, required this.isMe});

  @override
  Widget build(BuildContext context) {
    return Align(
      alignment: isMe ? Alignment.centerRight : Alignment.centerLeft,
      child: Container(
        margin: const EdgeInsets.symmetric(vertical: 5),
        padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 15),
        decoration: BoxDecoration(
          color: isMe ? Colors.deepPurple[100] : Colors.grey[300],
          borderRadius: BorderRadius.circular(20),
        ),
        child: Column(
          crossAxisAlignment:
              isMe ? CrossAxisAlignment.end : CrossAxisAlignment.start,
          children: [
            Text(
              message.message,
              style: TextStyle(fontSize: 16),
            ),
            Text(
              DateFormat('hh:mm a').format(message.sentAt),
              style: TextStyle(fontSize: 12, color: Colors.grey[600]),
            ),
          ],
        ),
      ),
    );
  }
}
