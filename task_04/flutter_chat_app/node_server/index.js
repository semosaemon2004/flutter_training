const express = require("express");
const { createServer } = require("node:http");
const { join } = require("node:path");
const { Server } = require("socket.io");

const app = express();
const server = createServer(app);
const io = new Server(server);

const messages = [];

io.on("connection", (socket) => {
  const username = socket.handshake.query.username;
  socket.on("message", (data) => {
    const message = {
      message: data.message,
      senderUsername: username,
      sentAt: Date.now(),
    };
    messages.push(message);
    io.emit("message", message);
  });
});

server.listen(3000, () => {
  console.log("server running at http://localhost:3000");
});
