import 'package:flutter/material.dart';
import 'package:flutter_cube/flutter_cube.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({super.key});

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  late Object plane;
  @override
  void initState() {
    plane = Object(fileName: "assets/plane/PUSHILIN_Plane.obj");
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.orange,
        title: Text("3D App"),
        centerTitle: true,
      ),
      body: Container(
        child: Cube(
          onSceneCreated: (Scene scene) {
            scene.world.add(plane);
            scene.camera.zoom = 6;
          },
        ),
      ),
    );
  }
}
